import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {



    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] nd = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nd[0]);

        int d = Integer.parseInt(nd[1]);

        int[] a = new int[n];

        String[] aItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int aItem = Integer.parseInt(aItems[i]);
            a[i] = aItem;
        }

        d = d % n ;
        int temp[] = new int[d];
        for(int i = 0; i < d; i++) {
            temp[i] = a[i];
        }
        for(int i = 0; i < n - d; i++) {
            a[i] = a[i+d];
            System.out.print(a[i] + " ");
        }
        for(int i = n - d - 1; i < n-1; i++) {
            a[i] = temp[i - (n - d - 1)];
            System.out.print(a[i] + " ");
        }
        
        scanner.close();
    }
}
