import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static HashMap<Integer,Node> map = new HashMap<Integer,Node>();
    static int[] size;
    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int noOfInputs = Integer.parseInt(br.readLine());
        size = new int[(2*noOfInputs)+1];
        size[1]=2*noOfInputs;
        for(int i=1;i<=noOfInputs;i++){
            String[] strArr = br.readLine().split(" ");
            
            int index1 = Integer.parseInt(strArr[0]);
            Node node1 = map.get(index1);
            if(node1==null)
            {
                node1 = new Node(index1,1);
                node1.parent = node1;
                map.put(index1, node1);
            }
            
            int index2 = Integer.parseInt(strArr[1]);
            Node node2 = map.get(index2);
            if(node2==null)
            {
                node2 = new Node(index2,1);
                node2.parent = node2;
                map.put(index2, node2);
            }
            
            merge(index1,index2);
        }
        
        int min = Integer.MAX_VALUE;
        for(int i=2;i<size.length;i++)
        {
            if(size[i]>0)
            {
                min = i;
                break;
            }
        }
        
        int max = Integer.MIN_VALUE;
        for(int i=size.length-1;i>=2;i--)
        {
            if(size[i]>0)
            {
                max = i;
                break;
            }
        }
        System.out.println(min+" "+max);
    }

    private static void merge(int i, int j) {
        Node parent1 = findParent(i);
        Node parent2 = findParent(j);
        
        if(parent1==parent2)
            return;
        size[parent1.size]--;
        size[parent2.size]--;
        if(parent1.size >= parent2.size){
            parent2.parent = parent1;
            parent1.size+=parent2.size;
            size[parent1.size]++;
        }
        else
        {
            parent1.parent = parent2;
            parent2.size+=parent1.size;
            size[parent2.size]++;
        }
    }

    private static Node findParent(int i) {
        
        return findParent(map.get(i)).parent;
    }

    private static Node findParent(Node node) {    
        return node.parent == node ? node : (node.parent = findParent(node.parent));
    }
}

class Node{
    Node parent;
    int data;
    int size;
    public Node(int data, int size) {
        super();
        this.data = data;
        this.size = size;
    }
}